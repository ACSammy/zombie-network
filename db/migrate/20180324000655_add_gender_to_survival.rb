class AddGenderToSurvival < ActiveRecord::Migration[5.1]
  def change
    add_column :survivals, :gender, :string, limit: 1
  end
end
