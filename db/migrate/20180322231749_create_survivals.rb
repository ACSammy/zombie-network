class CreateSurvivals < ActiveRecord::Migration[5.1]
  def change
    create_table :survivals do |t|
      t.string :name
      t.integer :year_birthday
      t.integer :situation, default: 0
      t.string :latitude
      t.string :longitude
      t.integer :resource1
      t.integer :resource2
      t.integer :resource3
      t.integer :resource4

      t.timestamps
    end
  end
end
