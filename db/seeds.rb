# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
puts "Creating Survivors -----------------------"
250.times do
  Survival.create!(
    name: Faker::Name.name,
    year_birthday: Faker::Number.between(1950, 2018),
    gender: ["f", "m", "f", "m"].sample,
    situation: [0,0,0,0,0,0,0,1].sample,
    longitude: Faker::Address.longitude,
    latitude: Faker::Address.latitude,
    resource1:  Faker::Number.between(1, 20),
    resource2:  Faker::Number.between(1, 20),
    resource3:  Faker::Number.between(1, 20),
    resource4:  Faker::Number.between(1, 20)
  )
end
puts "----------------------- Creating Survivors "
