class Survival < ApplicationRecord
  enum situation: [:alive, :infected]

  scope :non_infected, -> {where(situation: :alive)}
  scope :infected, -> {where(situation: :infected)}

  def self.search(search)
    where("name LIKE ?", "%#{search}%")
  end

end
