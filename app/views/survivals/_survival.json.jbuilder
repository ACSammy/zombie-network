json.extract! survival, :id, :name, :year_birthday, :status, :latitude, :longitude, :resource1, :resource2, :resource3, :resource4, :created_at, :updated_at
json.url survival_url(survival, format: :json)
