class HomeController < SurvivalsController
  def index
    @survival_total = Survival.all.size.to_i
    @non_infected = Survival.where(situation: :non_infected).size.to_i
    @infected = Survival.where(situation: :infected).size.to_i
    water = 0
    food = 0
    medication = 0
    ammunation = 0
    survivors = Survival.all

    survivors.each do |survivor|
      water = water + survivor.resource1.to_i
      food = food + survivor.resource2.to_i
      medication = medication + survivor.resource3.to_i
      ammunation = ammunation + survivor.resource4.to_i
    end

    @water = water/@survival_total.to_f
    @food = food/@survival_total.to_f
    @medication = medication/@survival_total.to_f
    @ammunation = ammunation/@survival_total.to_f

  end

  def info
    msg = { non_infected: @non_infected, infected: @infected, water: @water, food: @food, medication: @medication, ammunation: @ammunation}
    render json: msg
  end
end
