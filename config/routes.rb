Rails.application.routes.draw do
  get 'survivals/apocalypse'

  get 'survival/apocalypse'

  root to: 'survivals#index'

  get "apocalypse", to: 'home#index'

  resources :survivals
  resources :resources
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
